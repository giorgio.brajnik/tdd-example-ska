# Requirements of the example

(_this is the starting point of the exercise; we will develop together the code that will implement these requirements_)

1. Provide a Calc class that can be used to perform additions, subtractions, multiplications and divisions. 
1. Additions and multiplications shall accept multiple arguments.
1. Division shall return a float, and possibly an exception when dividing by 0.
1. Another operation is averaging numbers in a iterable like a list.
  This function should allow to filter out outliers (from low and from high end).
