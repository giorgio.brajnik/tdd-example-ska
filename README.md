# TDD workshop for SKA

To participate to the workshop you need to have pycharm running and a Python virtual environment with python 3.5 installed as well as some ancillary system.
A little bit of experience in python programming is needed. 

## Technical prerequisites for running this example

* I'm using pycharm-2019.2.3 CE
* python 3.5.2
* virtualenv 16.7.5
* pytest-5.2.0

## What you need to prepare before the meeting

After cloning the repo `git@gitlab.com:giorgio.brajnik/tdd-example-ska.git`
install the prerequisite and then perform the following steps:

```bash
pip install --user pipenv
cd tdd-python-ska
virtualenv venv -p /usr/bin/python3.5
pipenv install -r requirements/dev.txt
pipenv install -r requirements/test.txt
```
Configure also PyCharm:
* and made sure that the python interpreter chosen by PyCharm is the one on the virtual environment of this project,
namely `Settings >> Project >> Project Interpreter = ...../tdd-python-ska/venv/bin/python3`
* then I set the appropriate test runner to pytest: `Settings >> Tools >> Python Integrated Tools >> default test runner = pytest`

## The workshop starts here

Using pycharm open the file docs/example-requirements.md. Read it and join the meeting. Do not write any code before joining.
