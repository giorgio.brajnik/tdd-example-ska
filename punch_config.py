__config_version__ = 1


GLOBALS = {
    'serializer': '{{major}}.{{minor}}.{{patch}}',
}


FILES = ["setup.py", "python_boilerplate/__init__.py"]

VERSION = ['major', 'minor', 'patch']
